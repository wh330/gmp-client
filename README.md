A GMP client to manage resources on a remote GVM daemon via TLS connection. It will be essentially used to implement the logic of the Friendly Probing Suite.
